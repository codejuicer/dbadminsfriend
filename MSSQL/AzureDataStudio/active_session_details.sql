-------------------------------------------------------------------------------
-- Active Session Details
-------------------------------------------------------------------------------
-- Nothing too fancy here. It provides details on running sessions that may be
-- helpful. SQL commands along with their handle, and the query plan handle.
--
-- Released under GPL 3.0 license.
-------------------------------------------------------------------------------
SELECT s.session_id [Session ID],
    s.original_login_name [User],
    s.program_name [Program],
    st.text AS [Command],
    CASE s.transaction_isolation_level
       WHEN 0 THEN 'Unspecified'
       WHEN 1 THEN 'Read Uncomitted'
       WHEN 2 THEN 'Read Comitted'
       WHEN 3 THEN 'Repeatable'
       WHEN 4 THEN 'Serializable'
       WHEN 5 THEN 'Snapshot'
       END AS [Isolation Level],
    p.blocked [Blocked],
    p.cpu [CPU Usage],
    p.sql_handle [SQL Handle],
    plan_handle [Query Plan Handle]
FROM sys.sysprocesses p
    INNER JOIN sys.dm_exec_sessions s
    ON s.session_id = p.spid
        AND s.[status] = 'running'
    INNER JOIN sys.dm_exec_requests req
        ON req.session_id = s.session_id
CROSS APPLY sys.dm_exec_sql_text(p.sql_handle) st
;

