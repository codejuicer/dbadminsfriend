-------------------------------------------------------------------------------
-- Session Length Report
-------------------------------------------------------------------------------
-- Created for use with Azure Data Studio to provide session length details.
-- Sometimes it's nice to know how active sessions really are. This query
-- calculates the length of session connection, as well as the time since the
-- last action completed.
--
-- Normally the time calculations will be in DDd HH:MM:SS format. Time difference
-- For example: 2d 02:32:10
-- If you are wondering why "DATEPART" wasn't used for these calculations, it's
-- because datepart sometimes rounds up. That makes calculations a pain.
--
-- sa user, along with some typical Azure SQL programs are filtered to avoid
-- noise with Azure SQL instances.
--
-- Released under GPL 3.0 license.
-------------------------------------------------------------------------------
DECLARE @Now DATETIME2 = CURRENT_TIMESTAMP;
SELECT session_id,
       original_login_name [User],
       program_name [Program],
       [status],
       --login_time,
       CONCAT(
           CONCAT(RIGHT(CONCAT('00',CAST(datediff(SECOND,login_time,@Now)/86400 AS VARCHAR)),2),'d '),
           CAST(timefromparts(
           datediff(SECOND,login_time,@Now)/3600 - (datediff(SECOND,login_time,@Now)/86400)*24,
           datediff(SECOND,login_time,@Now)/60 - (datediff(SECOND,login_time,@Now)/3600)*60,
           datediff(SECOND,login_time,@Now) - (datediff(SECOND,login_time,@Now)/60)*60
           ,0,0) AS VARCHAR)
       ) [Session Time HH:MM:SS],
       CONCAT(CONCAT(RIGHT(CONCAT('00',CAST(datediff(SECOND,last_request_end_time,@Now)/86400 AS VARCHAR)),2),'d '),
           CAST(timefromparts(
           datediff(SECOND,last_request_end_time,@Now)/3600 - (datediff(SECOND,last_request_end_time,@Now)/86400)*24,
           datediff(SECOND,last_request_end_time,@Now)/60 - (datediff(SECOND,last_request_end_time,@Now)/3600)*60,
           datediff(SECOND,last_request_end_time,@Now) - (datediff(SECOND,last_request_end_time,@Now)/60)*60
           ,0,0) AS VARCHAR)) [Time Since Last Action]
FROM sys.dm_exec_sessions
WHERE original_login_name NOT IN('sa', 'dpa')
      AND program_name NOT IN ('MetricsDownloader', 'TdService', 'BackupService', 'DmvCollector')
ORDER BY 5 DESC;
